// Soal 1

// Luas persegi panjang
const luaspersegipanjang = (p, l) => p*l;

// Keliling persegi panjang
const kelilingpersegipanjang = (p, l) => 2*(p+l);

let panjang = 7;
let lebar = 3;

console.log(luaspersegipanjang(panjang, lebar));
console.log(kelilingpersegipanjang(panjang, lebar));

// Soal 2
const newFunction = (NamaDepan, NamaBelakang) => {
	return {
		NamaDepan: NamaDepan,
		NamaBelakang: NamaBelakang,
		NamaLengkap: () => console.log(`${NamaDepan}, ${NamaBelakang}`)
	}
}
newFunction("Hastri", "Cantya").NamaLengkap()

// Soal 3
const soal3 = {
	NamaDepan: 'Hastri',
	NamaBelakang : 'Cantya',
	Alamat : 'Bandung',
	Hobi : 'Memasak'	
}
const {NamaDepan, NamaBelakang, Alamat, Hobi} = soal3
console.log(NamaDepan, NamaBelakang
	, Alamat, Hobi)

// Soal 4
const data1 = ["Hastri", "Cantya", "Bandung", "Memasak"]
const data2 = ["Cantya", "Hastri", "Surabaya", "Istirahat"]
const gabung = [...data1, ...data2]

console.log(gabung)

// Soal 5
const planet = "bumi"
const view = "teleskop"

const galaksi = `Alat melihatnya ${view}, yang ada di ${planet}`
console.log(galaksi)