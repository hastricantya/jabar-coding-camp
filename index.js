// latihan
var sayHello = "Hello World!"
console.log(sayHello)

// soal 1
var pertama = "Saya sangat senang hari ini ";
var kedua = "belajar javascript itu keren";
// jawaban soal 1
var soal1 = pertama + kedua
console.log(soal1)

// soal 2
// kalau integer atau number gapakai petik dua
var kataPertama = 10; 
var kataKedua = 2;
var kataKetiga = 4;
var kataKeempat = 6;
// jawaban soal 2
var soal2 = kataPertama + kataKedua * kataKetiga + kataKeempat
console.log(soal2)

// soal 3
var kalimat = 'wah javascript itu keren sekali '; 

var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substring(4, 14); 
var kataKetiga = kalimat.substring(15, 18); 
var kataKeempat = kalimat.substring(19, 24);  
var kataKelima = kalimat.substring(25, 31); // 

// jawaban soal 3
console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);