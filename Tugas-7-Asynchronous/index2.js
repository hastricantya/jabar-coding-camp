var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'NKCTHI', timeSpent: 3000}, 
    {name: 'Its Okay to Be Not Okay', timeSpent: 2000}, 
    {name: 'Catatan Juang', timeSpent: 4000},
    {name: 'Garis Waktu', timeSpent: 1000}
]

const readPromise = () => {return readBooksPromise(10000, books[0])
    .then(time=>{return readBooksPromise(time, books[1])
        .then(time=>{return readBooksPromise(time, books[2])
            .then(time=>{return readBooksPromise(time, books[3])
                .catch(time=>{return time})
            }
        )}
    )}
)}

readPromise();