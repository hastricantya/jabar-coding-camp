// Quiz

// Soal 1
function jumlah_kata(str){
    var HapusSpasi = str.trimStart().trimEnd()
    var newArray = HapusSpasi.split(" ")
    var HitungKata = newArray.length
    return HitungKata;
}

var kalimat_1 = jumlah_kata(" Halo nama Saya Hastri Cantya "); 
var kalimat_2 = jumlah_kata("Saya Hastri");
console.log(kalimat_1, kalimat_2)
// hasil jumlah kalimat_1 = 5
// hasil jumlah kalimat_2 = 2


// Soal 2
function next_date(tanggal, bulan, tahun){
    var haribesok = tanggal + 1
    var inputbulan = bulan
    var inputtahun = tahun
    if (haribesok == 30) {
        haribesok = 1
        switch(inputbulan + 1) {
            case 1:
                namabulan =  "Januari";
                break;
            case 2:
                namabulan = "Februari";
                break;
            case 3:
                namabulan = "Maret";
                break;
            case 4:
                namabulan = "April";
                break;
            case 5:
                namabulan = "Mei";
                break;
            case 6:
                namabulan = "Juni";
                break;
            case 7:
                namabulan = "Juli";
                break;
            case 8:
                namabulan = "Agustus";
                break;
            case 9:
                namabulan = "September";
                break;
            case 10:
                namabulan = "Oktober";
                break;
            case 11:
                namabulan = "November";
                break;
            default:
                namabulan = "Desember";
        }
    } else if (haribesok == 29 && inputtahun == 2021) {
        haribesok = 1
        switch(inputbulan + 1) {
            case 1:
                namabulan =  "Januari";
                break;
            case 2:
                namabulan = "Februari";
                break;
            case 3:
                namabulan = "Maret";
                break;
            case 4:
                namabulan = "April";
                break;
            case 5:
                namabulan = "Mei";
                break;
            case 6:
                namabulan = "Juni";
                break;
            case 7:
                namabulan = "Juli";
                break;
            case 8:
                namabulan = "Agustus";
                break;
            case 9:
                namabulan = "September";
                break;
            case 10:
                namabulan = "Oktober";
                break;
            case 11:
                namabulan = "November";
                break;
            default:
                namabulan = "Desember";
        }
    } else if (haribesok == 32 && inputbulan == 12) {
        haribesok = 1
        switch(inputbulan - 11) {
            case 1:
                namabulan =  "Januari";
                break;
            case 2:
                namabulan = "Februari";
                break;
            case 3:
                namabulan = "Maret";
                break;
            case 4:
                namabulan = "April";
                break;
            case 5:
                namabulan = "Mei";
                break;
            case 6:
                namabulan = "Juni";
                break;
            case 7:
                namabulan = "Juli";
                break;
            case 8:
                namabulan = "Agustus";
                break;
            case 9:
                namabulan = "September";
                break;
            case 10:
                namabulan = "Oktober";
                break;
            case 11:
                namabulan = "November";
                break;
            default:
                namabulan = "Desember";
        }
       inputtahun += 1
    } else {
        switch(inputbulan) {
            case 1:
                namabulan =  "Januari";
                break;
            case 2:
                namabulan = "Februari";
                break;
            case 3:
                namabulan = "Maret";
                break;
            case 4:
                namabulan = "April";
                break;
            case 5:
                namabulan = "Mei";
                break;
            case 6:
                namabulan = "Juni";
                break;
            case 7:
                namabulan = "Juli";
                break;
            case 8:
                namabulan = "Agustus";
                break;
            case 9:
                namabulan = "September";
                break;
            case 10:
                namabulan = "Oktober";
                break;
            case 11:
                namabulan = "November";
                break;
            default:
                namabulan = "Desember";
        }
    }
    var namabulan;
    return haribesok + " " + namabulan + " " + inputtahun
}

// output contoh 1

var tanggal = 29
var bulan = 2
var tahun = 2020

console.log(next_date(tanggal , bulan , tahun )) // output : 1 Maret 2020

// output contoh 2

var tanggal = 28
var bulan = 2
var tahun = 2021

console.log(next_date(tanggal , bulan , tahun )) // output : 1 Maret 2021

// output contoh 3

var tanggal = 31
var bulan = 12
var tahun = 2020

console.log(next_date(tanggal , bulan , tahun )) // output : 1 Januari 2021
