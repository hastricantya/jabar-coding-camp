// SOAL 1
// Konversi nilai
var nilai = 90

if(nilai >= 85 ){
     console.log('A');
 } else if (nilai >= 75 ){
     console.log('B');
 } else if (nilai >= 65){
     console.log('C');
 } else if (nilai >= 55){
     console.log('D');
 } else {
     console.log('E');
 }

// SOAL 2
// Kelahiran
 var tanggal = 25;
 var bulan = 9;
 var tahun = 2001;

 switch(bulan){
    case 1: bulan = "Januari"; break;
    case 2: bulan = "Februari"; break;
    case 3: bulan = "Maret"; break;
    case 4: bulan = "April"; break;
    case 5: bulan = "Mei"; break;
    case 6: bulan = "Juni"; break;
    case 7: bulan = "Juli"; break;
    case 8: bulan = "Agustus"; break;
    case 9 : bulan = "September"; break;
    case 10: bulan = "Oktober"; break;
    case 11: bulan = "November"; break;
    case 12: bulan = "Desember"; break;
}
var lahirku = "Lahir :" +tanggal+ " "+ bulan + " " + tahun;
console.log (lahirku);

//SOAL 3
//Segitiga siku"

// n=3
console.log('\n');
var segitiga = '';
var banyak = 3;

for (var i = 1; i <= banyak; i++) {
    for (var j = 1; j <= i; j++) {
        segitiga += '#';
    }
    segitiga += '\n';
}

console.log(segitiga);

// n = 7
console.log('\n');
var segitiga = '';
var banyak = 7;

for (var i = 1; i <= banyak; i++) {
    for (var j = 1; j <= i; j++) {
        segitiga += '#';
    }
    segitiga += '\n';
}

console.log(segitiga);

// Soal 4
// Looping love programming, js, vuejs

// m = 3
var m = 3;
var banyak = 1;
var line = '';
var batas = '';

for(var i=1; i <= m; i++){
    if(banyak > 3){
        banyak = 1;
    }

    switch(banyak){
        case 1: line += i + ' - I Love Programming \n'; break;
        case 2: line += i + ' - I Love JavaScript \n'; break;
        case 3: batas += '==='; line += i + ' - I Love Vue.js \n ' + batas + '\n'; break;
    }

    banyak++;
}
console.log('\n' + line + '\n');

// m = 7
var m = 7;
var banyak = 1;
var line = '';
var batas = '';

for(var i=1; i <= m; i++){
    if(banyak > 3){
        banyak = 1;
    }

    switch(banyak){
        case 1: line += i + ' - I Love Programming \n'; break;
        case 2: line += i + ' - I Love JavaScript \n'; break;
        case 3: batas += '==='; line += i + ' - I Love Vue.js \n ' + batas + '\n'; break;
    }

    banyak++;
}

console.log('\n' + line + '\n');

// m = 10
var m = 10;
var banyak = 1;
var line = '';
var batas = '';

for(var i=1; i <= m; i++){
    if(banyak > 3){
        banyak = 1;
    }

    switch(banyak){
        case 1: line += i + ' - I Love Programming \n'; break;
        case 2: line += i + ' - I Love JavaScript \n'; break;
        case 3: batas += '==='; line += i + ' - I Love Vue.js \n ' + batas + '\n'; break;
    }

    banyak++;
}

console.log('\n' + line + '\n');

