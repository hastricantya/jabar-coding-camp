// Soal 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"]
var mengurutkan = daftarHewan.sort()
mengurutkan.forEach (function(hewan) {
	console.log(hewan)
}); 

// Soal 2
var data = {name : "Hastri" , age : 19 , address : "Bandung" , hobby : "Masak" }
function introduce(aku){
	var nama = "Nama Saya "+ aku.name+ ", ";
	var usia = "umur Saya "+ aku.age+ ", ";
	var alamat = "alamat Saya di "+ aku.address+ ", ";
	var hobi = "dan Saya punya hobi, yaitu "+ aku.hobby+ ".";
	return nama + usia + alamat + hobi
} 
var introduce = introduce(data)
console.log(introduce)

// Soal 3
function hitung_huruf_vokal(huruf){
	huruf.split("");
	var cekvokal = ["a", "i", "u", "e", "o"]
	var penjumlahanvokal = 0;
	for(var i = 0; i < huruf.length; i++){
		if (cekvokal.indexOf(huruf.toLowerCase()[i]) !== -1){
			penjumlahanvokal++}
	}
	return penjumlahanvokal;
}

var hitung_1 = hitung_huruf_vokal("Hastri")
var hitung_2 = hitung_huruf_vokal("Cantya")
console.log (hitung_1, hitung_2)

// Soal 4 
function hitung (cekangka) {
	return (cekangka - 1) * 2
}

console.log( hitung(0) ) // -2
console.log( hitung(1) ) // 0
console.log( hitung(2) ) // 2
console.log( hitung(3) ) // 4
console.log( hitung(5) ) // 8